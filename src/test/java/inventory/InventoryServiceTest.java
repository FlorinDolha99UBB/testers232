package inventory;

import inventory.model.InhousePart;
import inventory.repository.InventoryRepository;
import org.junit.jupiter.api.Tag;
import org.junit.platform.commons.annotation.Testable;


class InventoryServiceTest {
    private InventoryRepository repo;

    @org.junit.jupiter.api.BeforeEach
    void setUp() {
        repo = new InventoryRepository();
    }

    @org.junit.jupiter.api.AfterEach
    void tearDown() {
    }

    @org.junit.jupiter.api.RepeatedTest(10)
    @org.junit.jupiter.api.Test
    void addInhousePartECP1() {
        InhousePart inhousePart = new InhousePart(repo.getAutoPartId(), "TestName", 2, 5, 0, 200, 10);
        assert !inhousePart.getName().isEmpty();
        assert inhousePart.getPrice()>0;
        assert inhousePart.getInStock()>0;
    }

    @org.junit.jupiter.api.Order(1)
    @org.junit.jupiter.api.Test
    void addInhousePartECP2() {
        InhousePart inhousePart = new InhousePart(repo.getAutoPartId(), "TestName", 3, 0, 0, 200, 10);
        assert !inhousePart.getName().isEmpty();
        assert inhousePart.getPrice()>0;
        assert inhousePart.getInStock()<=0;
    }

    @org.junit.jupiter.api.Timeout(10)
    @org.junit.jupiter.api.Test
    void addInhousePartECP3() {
        InhousePart inhousePart = new InhousePart(repo.getAutoPartId(), "TestName", -2, 6, 0,200,10);
        assert !inhousePart.getName().isEmpty();
        assert inhousePart.getPrice()<=0;
        assert inhousePart.getInStock()>0;
    }

    @Testable
    @Tag("ECP4")
    @org.junit.jupiter.api.Test
    void addInhousePartECP4() {
        InhousePart inhousePart = new InhousePart(repo.getAutoPartId(), "", 6, 2, 0, 200,10);
        assert inhousePart.getName().isEmpty();
        assert inhousePart.getPrice()>0;
        assert inhousePart.getInStock()>0;
    }

}