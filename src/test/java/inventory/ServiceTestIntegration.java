package inventory;

import inventory.repository.InventoryRepository;
import inventory.service.InventoryService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
public class ServiceTestIntegration {
    @Spy
    private InventoryRepository inventoryRepository;
    @InjectMocks
    private InventoryService inventoryService;

    @Test
    public void addProduct() {
        inventoryService.addProduct("Name",20.00,2,4,8,null);
        Mockito.verify(inventoryRepository, times(1)).addProduct(any());
    }

    @Test
    public void deleteProduct() {
        int size = inventoryRepository.getAllProducts().size();
        inventoryService.addProduct("Name",20.00,2,4,8,null);
        inventoryService.deleteProduct(inventoryService.lookupProduct("Name"));
        assert inventoryService.getAllProducts().size()==size;
    }
}

