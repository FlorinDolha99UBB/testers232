package inventory;

import inventory.model.Product;
import inventory.repository.InventoryRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class InventoryRepositoryTestMockito {
    @Spy
    private InventoryRepository inventoryRepository;

    Product prod = new Product(1, "Name", 20.00, 2, 5, 10,null);


    @Test
    void addProduct() {
        int size = inventoryRepository.getAllProducts().size();
        inventoryRepository.addProduct(prod);
        assert inventoryRepository.getAllProducts().size()==size+1;
    }

    @Test
    void deleteProduct() {
        int size = inventoryRepository.getAllProducts().size();
        inventoryRepository.addProduct(prod);
        assert inventoryRepository.getAllProducts().size()==size+1;
        inventoryRepository.deleteProduct(prod);
        assert inventoryRepository.getAllProducts().size()==size;
    }
}