package inventory;

import inventory.model.InhousePart;
import inventory.model.Part;
import inventory.repository.Inventory;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InventoryTest {
    private Inventory inventory;
    Part part1 = new InhousePart(1,"part1",1.00,10,1,100,1);
    Part part2 = new InhousePart(2,"part2",1.00,10,1,100,1);
    Part part3 = new InhousePart(3,"part3",2.00,10,1,100,1);
    Part part4 = new InhousePart(4,"part4",5.00,10,1,100,1);
    Part part5 = new InhousePart(5,"part5",7.00,10,1,100,1);

    private Inventory inventory2;

    @BeforeEach
    void setUp() {
        inventory2 = new Inventory();
        inventory = new Inventory();
        inventory.addPart(part1);
        inventory.addPart(part2);
        inventory.addPart(part3);
        inventory.addPart(part4);
        inventory.addPart(part5);
    }

    @AfterEach
    void tearDown() {
        inventory.deletePart(part1);
        inventory.deletePart(part2);
        inventory.deletePart(part3);
        inventory.deletePart(part4);
        inventory.deletePart(part5);
    }

    @Test
    void lookupPartValid1() {
        assert inventory.lookupPart("2").equals(part2);
    }

    @Test
    void lookupPartNonvalid1() {assert inventory.lookupPart(null) == null;}

    @Test
    void lookupPartValid() {
        assert inventory.lookupPart("part1").equals(part1);
    }

    @Test
    void lookupPartNonValid() {
        assert inventory.lookupPart("prt") == null;
    }

    @Test
    void lookupPartNonvalid2() {assert inventory2.lookupPart("part1") == null;}
}