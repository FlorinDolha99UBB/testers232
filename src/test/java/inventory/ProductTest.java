package inventory;

import inventory.model.Product;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

class ProductTest {
    Product product = new Product(1,"Name",20.00, 10, 4, 20, null);

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @org.junit.jupiter.api.Order(1)
    @org.junit.jupiter.api.Test
    void setName() {
        assert product.getName().equals("Name");
        product.setName("Name2");
        assert product.getName().equals("Name2");
    }

    @org.junit.jupiter.api.Timeout(500)
    @org.junit.jupiter.api.Test
    void setPrice() {
        assert product.getPrice()==20.00;
        product.setPrice(30.50);
        assert product.getPrice()==30.50;
    }
}