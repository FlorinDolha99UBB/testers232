package inventory;

import inventory.repository.InventoryRepository;
import inventory.service.InventoryService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.mockito.Mockito.times;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
public class InventoryServiceTestIntegration {
    @Mock
    private InventoryRepository inventoryRepository;
    @InjectMocks
    private InventoryService inventoryService;

    @Test
    public void addProduct() {
        inventoryService.addProduct("Name",20.00,2,4,8,null);
        Mockito.verify(inventoryRepository, times(1)).addProduct(any());
    }

    @Test
    public void deleteProduct() {
        inventoryService.addProduct("Name",20.00,2,4,8,null);
        inventoryService.deleteProduct(inventoryService.lookupProduct("Name"));
        Mockito.verify(inventoryRepository, times(1)).deleteProduct(any());

    }
}
