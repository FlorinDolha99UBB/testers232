package inventory;

import inventory.model.Product;
import inventory.repository.InventoryRepository;
import inventory.service.InventoryService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class InventoryServiceTestMockito {
    @Spy
    private InventoryRepository inventoryRepository;

    @InjectMocks
    private InventoryService inventoryService;

    @Test
    void addProduct() {
        int size = inventoryService.getAllProducts().size();
        inventoryService.addProduct("Name",20.00,2,4,8,null);
        assert inventoryService.getAllProducts().size()==size+1;
    }

    @Test
    void deleteProduct() {
        int size = inventoryService.getAllProducts().size();
        inventoryService.addProduct("Name",20.00,2,4,8,null);
        assert inventoryService.getAllProducts().size()==size+1;
        inventoryService.deleteProduct(inventoryService.lookupProduct("Name"));
        assert inventoryService.getAllProducts().size()==size;
    }
}